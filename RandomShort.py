import string
import random
from urllib import parse

import webApp
import shelve


contents = shelve.open('contents')

FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Introduce an URL to short: </label>
        <input type="text" name="URL" required>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""
PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""
PAGE_FORM = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>MAIN PAGE:</p>
    </div>
    <div>
      {form}
    </div>
    <div>
      <p>URL LIST:<br> {urllist}</p>
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESSABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""


PAGE_POST = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
    <p>Shorten url: {body}.</p>
    </div>
    <div>
      {form}
    </div>
    <div>
      <p>URL LIST:<br> {urllist}</p>
    </div>
  </body>
</html>
"""


class RandomShort(webApp.WebApp):

    def parse(self, request):
        """Return the method name and resource name"""

        dic = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            dic['body'] = None
        else:
            dic['body'] = request[body_start + 4:]
        parts = request.split(' ', 2)
        dic['method'] = parts[0]
        dic['resource'] = parts[1]
        return dic

    def process(self, data):
        """Produce the page with the content for the resource"""

        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            code, page = "405 Method not allowed", \
                         PAGE_NOT_ALLOWED.format(method=data['method'])
        return code, page

    def get(self, resource):

        if resource == '/':
            page = PAGE_FORM.format(form=FORM, urllist=self.fulldic(contents))
            code = "200 OK"

        elif resource in contents:
            url = contents[resource]
            page = ""
            code = '301 Moved Permanently\r\nLocation: {location}\r\n\r\n'.format(location=url)
        elif resource == '/favicon.ico':
            code = "204 No content"
            page = ""
        else:
            code = "404 Not found"
            page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)

        return code, page

    def post(self, resource, body):
        args = parse.parse_qs(body)
        print(args)
        if resource == '/':
            if 'URL' in args:
                url = self.fullurl(args['URL'][0])
                if url in contents.values():
                    shorten_url = list(contents.keys())[list(contents.values()).index(url)]
                    print(shorten_url)
                    page = PAGE_POST.format(body=shorten_url, form=FORM, urllist=self.fulldic(contents))
                    code = "200 Ok"

                else:
                    random_key = ''.join(random.choices(string.ascii_lowercase + string.digits, k=8))
                    shorten_url = '/' + random_key
                    contents[shorten_url] = url
                    print(shorten_url)
                    page = PAGE_POST.format(body=shorten_url,form=FORM, urllist=self.fulldic(contents))
                    code = "200 Ok"
            else:
                page = PAGE_UNPROCESSABLE.format(body=body)
                code = "422 Unprocessable Entity"
        else:
            page = PAGE_UNPROCESSABLE.format(body=body)
            code = "422 Unprocessable Entity"
        return code, page

    def fullurl(self, url):
        if "http://" in url or "https://" in url:
            return url
        else:
            return "http://" + url

    def fulldic(self, content):
        dic = ""
        for element in content:
            dic = dic + "Long Url: " + content[element] + " Shorten Url: " + element + "<br>"
        return dic


if __name__ == "__main__":
    webApp = RandomShort("localhost", 1234)
